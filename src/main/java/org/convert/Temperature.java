package org.convert;

import java.io.*;
import java.text.ParseException;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class Temperature extends HttpServlet {

    private org.convert.Convertor convertor;

    public void init() throws ServletException {
        convertor = new org.convert.Convertor();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Convertor";
        String fromTempUnit = request.getParameter("from");
        String toTempUnit = request.getParameter("to");
        try {
            Double temp = Double.parseDouble(request.getParameter("temperature"));
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Your Temperature of " + temp +" º"+ fromTempUnit +" is: \n" +
                    convertor.convert(fromTempUnit, toTempUnit, temp) +
                    "</BODY></HTML>");
        } catch (NullPointerException z) {
                out.println("<!DOCTYPE HTML>\n" +
                        "<HTML>\n" +
                        "<HEAD><TITLE>" + title + "</TITLE>" +
                        "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                        "</HEAD>\n" +
                        "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                        "<H1>" + title + "</H1>\n" +
                        "  <P>! At least one input is missing (Conversion units or temperature to be converted) !</P> \n" +
                        "</BODY></HTML>");
            } catch (NumberFormatException e) {
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Wrong input for temperature: " + request.getParameter("temperature") + "</P> \n" +
                    "</BODY></HTML>");
        }
    }
}