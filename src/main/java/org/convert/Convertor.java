package org.convert;

public class Convertor {

    public String convert (String fromTempType, String toTempType, Double fromTemp) {
        switch (fromTempType) {
            case "F" :
                switch (toTempType) {
                    case "K":
                        return String.format("%.3f", fahrenheitToKelvin(fromTemp)) + " K";
                    case "C":
                        return String.format("%.3f", fahrenheitToCelsius(fromTemp)) +" ºC";
                    default:
                        return fromTemp.toString()+" ºF";
                }

            case "C" :
                switch (toTempType) {
                    case "K":
                        return String.format("%.3f", celsiusToKelvin(fromTemp)) + " K";
                    case "F":
                        return String.format("%.3f", celsiusToFahrenheit(fromTemp)) + " ºF";
                    default:
                        return fromTemp.toString() + " ºC";
                }

            case "K":
                switch (toTempType) {
                    case "K":
                        return String.format("%.3f", kelvinToCelsius(fromTemp)) + " ºC";
                    case "F":
                        return String.format("%.3f", kelvinToFahrenheit(fromTemp)) + " ºF";
                    default:
                        return fromTemp.toString() + " K";
                }
            default:
                return "";
        }
    }

    private static Double celsiusToFahrenheit (Double temp) {
        return ((temp/5)*9)+32;
    }

    private static Double fahrenheitToCelsius (Double temp) {
        return ((temp-32)*5)/9;
    }

    private static Double celsiusToKelvin (Double temp) {
        return temp+273.15;
    }

    private static Double kelvinToCelsius (Double temp) {
        return temp-273.15;
    }

    private static Double kelvinToFahrenheit (Double temp) {
        return celsiusToFahrenheit(kelvinToCelsius(temp));
    }

    private static Double fahrenheitToKelvin (Double temp) {
        return celsiusToKelvin(fahrenheitToCelsius(temp));
    }
}
